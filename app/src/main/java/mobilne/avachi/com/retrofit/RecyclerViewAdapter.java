package mobilne.avachi.com.retrofit;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    public static final String IMAGE_URL = "http://covers.openlibrary.org/b/id/";
    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView title;
        TextView author;
        ImageView image;

        public ViewHolder(View v){
            super(v);
            title = v.findViewById(R.id.titleText);
            author = v.findViewById(R.id.authorText);
            image = v.findViewById(R.id.imageView);
        }
    }

    protected Context context;
    private BookList bookList;
    JSONArray jsonArray;

    public RecyclerViewAdapter(Context context, BookList book){
        this.context = context;
        this.bookList = book;
        jsonArray = new JSONArray();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_detail, viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.title.setText(bookList.getEmployeeArrayList().get(i).getTitle());
        viewHolder.author.setText((bookList.getEmployeeArrayList().get(i).getAuthors()==null)?"":bookList.getEmployeeArrayList().get(i).getAuthors().get(0));
        String imageURL = IMAGE_URL + bookList.getEmployeeArrayList().get(i).getCover() + "-M.jpg";

        Picasso.with(context).load(imageURL).placeholder(R.drawable.ic_launcher_background).into(viewHolder.image);
    }

    @Override
    public int getItemCount() {
        if(bookList != null)
            return bookList.getEmployeeArrayList().size();
        return 0;
    }
}
