package mobilne.avachi.com.retrofit;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class BookList {

    @SerializedName("docs")
    private ArrayList<Book> employeeList = new ArrayList<>();

    public ArrayList<Book> getEmployeeArrayList() {
        return employeeList;
    }

    public void setEmployeeArrayList(ArrayList<Book> employeeArrayList) {
        this.employeeList = employeeArrayList;
    }
}
