package mobilne.avachi.com.retrofit;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface OpenlibraryBooks {

    @GET("search.json")
    Call<BookList> getBooks(@Query("q") String query);
}
