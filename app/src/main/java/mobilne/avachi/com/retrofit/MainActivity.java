package mobilne.avachi.com.retrofit;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private RecyclerViewAdapter recyclerViewAdapter;
    private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = findViewById(R.id.findButton);
        button.setOnClickListener(new View.OnClickListener() {
            Context context;
            @Override
            public void onClick(View v) {
                context = v.getRootView().getContext();
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View view = inflater.inflate(R.layout.find_element, null, false);

                new AlertDialog.Builder(context).setView(view).setTitle("Find").setPositiveButton("Find", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final EditText editText = view.findViewById(R.id.titleTextE);
                        OpenlibraryBooks openlibraryBooks = RetrofitInstance.getRetrofitInstance().create(OpenlibraryBooks.class);

                        Call<BookList> call = openlibraryBooks.getBooks(editText.getText().toString());
                        call.enqueue(new Callback<BookList>() {

                            @Override
                            public void onResponse(Call<BookList> call, Response<BookList> response) {
                                generateDataList(response.body());
                                Toast.makeText(MainActivity.this, "Good", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFailure(Call<BookList> call, Throwable t) {
                                Toast.makeText(MainActivity.this,"Something went wrong... Plese try later!", Toast.LENGTH_LONG).show();
                            }
                        });
                        dialog.cancel();;
                    }
                }).show();
            }
        });
    }

    private void generateDataList(BookList bookList) {
        recyclerView = findViewById(R.id.recycleView);
        recyclerViewAdapter = new RecyclerViewAdapter(this, bookList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(layoutManager);
       // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(recyclerViewAdapter);
    }
}
